<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <h2>This is a Password reset mail</h2>
    <p>Please click on  the link to reset the password <a href="{{url('reset_password',[$token])}}">Click here</a> </p>
</body>
</html>