@extends('header_footer')
@section('main_content')

<div class="row">
    <div class="col-md-4 text-light m-4 mx-auto bg-dark p-4">
    <form action="{{url('password_reset_form',[$token])}}" method='post'>
    @csrf
        <label for="">New Password</label>
        <input type="password" name="new_pass" class="form-control" required>
        <label for="">Confirm Password</label>
        <input type="password" name="conf_pass" class="form-control" required>
        <input type="submit" class="btn btn-success mt-4">
    </form>
    </div>
</div>

@endsection