@extends('header_footer')
@section('main_content')
    <div class="row">
        <div class="col-md-6 mx-auto mt-5 mb-5 p-5 bg-dark text-white">
            <h2 class="text-center">Edit Details</h2>
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{session()->get('error')}}
                </div>
            @endif
            <form action="{{url('edit_form',[$edit_details->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Full Name</label>
                <!-- <input type="hidden" class="form-control" name="row_id" value="{{$edit_details->id}}" id=""> -->
                <input type="text" class="form-control" name="full_name" value="{{$edit_details->full_name}}" id="" required>
                <label for="">Email</label>
                <input type="email" class="form-control" name="email" value="{{$edit_details->email}}" readonly id="" required>
                <label for="">Contact</label>
                <input type="number" class="form-control" name="contact_no" value="{{$edit_details->contact_no}}" id="" required>
                <label for="">D.O.B</label>
                <input type="date" class="form-control" name="dob" value="{{$edit_details->dob}}" id="" required>
                <div class="row">
                    <div class="col-md-4 mt-4">
                        <img src='{{asset("profile_image/$edit_details->profile_image")}}' class="img-thumbnail" alt="">
                    </div>
                    <div class="col-md-8">
                        <label for="">Profile Image</label>
                        <input type="file" name="profile_image" class="form-control">
                    </div>
                </div>
                <div class="text-center mt-4">
                    <button class="btn btn-success" >Update</button>
                    @if(Session::get('type') == 'admin')
                    @else
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#passwordchangemodal" class="btn btn-warning">Change Password</a>
                    @endif
                </div>
            </form>
        </div>
    </div>
    
    <div class="modal" id="passwordchangemodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Change Password</h2>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <form action="{{url('change_password')}}" method='post'>
                   @csrf
                    <label for="">Enter your Old Password</label>
                    <p class="text-center" id="message"></p>
                    <input type="password" class="form-control" name="old_password" required>
                    <hr>
                    <label for="">New Password</label>
                    <input type="password" class="form-control" name="new_password" required>
                    <label for="">Confirm Password</label>
                    <input type="password" class="form-control" name="confirm_password" required>
                    <button class="btn btn-success mt-4">Change Password</button>
                   </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

@endsection

