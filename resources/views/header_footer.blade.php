<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
</head>
<body>
    
    <div class="container-fluid">
    <!-- navbar -->
        <div class="row">
            <div class="col-md-12 px-0">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Navbar
                    <!-- {{request()->route('id')}} -->
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                    <li class="nav-item {{'/' == request()->path() ? 'active':''}}">
                        <a class="nav-link" href="{{url('')}}">Home</a>
                    </li>
                    @if(Session::has('email'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('logout')}}">Logout</a>
                    </li>
                    @else
                    <li class="nav-item {{'register' == request()->path() ? 'active':''}}">
                        <a class="nav-link" href="{{url('register')}}">Register</a>
                    </li>
                    <li class="nav-item {{'login' == request()->path() ? 'active':''}}">
                        <a class="nav-link" href="{{url('login')}}">Login</a>
                    </li>
                    @endif
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li> -->
                    </ul>
                    <!-- <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form> -->
                </div>
                </nav>
            </div>
        </div>
        <!-- navbar end -->
        @yield('main_content')
        <div class="row">
            <div class="col-md-12 bg-dark text-white p-4">
                <ul style="list-style-type:none">
                    <li>Home</li>
                </ul>
            </div>
        </div>
    </div>

</body>
</html>