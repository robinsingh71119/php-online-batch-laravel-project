@extends('header_footer')
@section('main_content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{session()->get('error')}}
                </div>
            @endif
        </div>
        <div class="col-md-6 mx-auto">
            <h2 class="text-center">Welcome to Laravel Index Page</h2>        
        </div> 
    </div>
    @if(Session::has('email'))
    <div class="row">
        <div class="col-md-10 mx-auto">
        @if(count($registration_data) == 0)
            <div class='alert alert-warning text-center'>There is no user Registered</div>
        @else
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr class="text-center">
                        <th>S no</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>D.O.B</th>
                        <th>Profile Image</th>
                        <th>Password</th>
                        <th colspan='2'>Action</th>
                    </tr>
                </thead>
                <tbody>
                @php 
                    $counter = 1 
                @endphp
                    @foreach($registration_data as $data)
                        <tr>
                            <th>{{$counter}}</th>
                            <th>{{$data->full_name}}</th>
                            <th>{{$data->email}}</th>
                            <th>{{$data->contact_no}}</th>
                            <th>{{$data->dob}}</th>
                            <th><img src='{{asset("profile_image/$data->profile_image")}}' class="img-fluid" alt="">  </th>
                            <th>***********</th>
                            <th class="btn-group">
                                <a href="{{url('edit_page',[$data->id])}}"><button class="btn btn-warning mr-3">Edit</button></a>
                            @if(Session::get('type') == 'admin')
                                <a href="{{url('delete_page',[$data->id])}}"><button class="btn btn-danger">delete</button></a>
                            @else
                            @endif
                            </th>
                        </tr>
                        <input type="hidden" value="{{$counter ++}}">
                    @endforeach
                </tbody>
            </table>
            @if(Session::get('type') == 'admin')
            @else
            <button class="btn btn-success" data-toggle="modal" data-target="#adddetailmodal">Add Detail</button>
            @endif
        @endif
        </div>
    </div>
        @if($user_details)
        <div class="alert alert-warning">
           
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr class="text-center">
                        <th>S no</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Job Profile</th>
                        <th>Company Name</th>
                        <th>Duration</th>
                        <th>Position Title</th>
                        <th>Location</th>
                        <th>Technologies</th>
                        <th>Work Description</th>
                        <th>Skill</th>
                    </tr>
                </thead>
                <tbody>
                @php 
                    $counter = 1 
                @endphp
                    @foreach($user_details as $userdata)
                        <tr>
                            <th>{{$counter}}</th>
                            <th>{{$userdata->fullname}}</th>
                            <th>{{$userdata->email}}</th>
                            <th>{{$userdata->job_profile}}</th>
                            <th>{{$userdata->company_name}}</th>
                            <th>{{$userdata->duration}}</th>
                            <th>{{$userdata->position_title}}</th>
                            <th>{{$userdata->location}}</th>
                            <th>{{$userdata->technologies}}</th>
                            <th>{{$userdata->work_description}}</th>
                            <th>{{$userdata->skill}}</th>
                            <!-- <th><img src='{{asset("profile_image/$data->profile_image")}}' class="img-fluid" alt="">  </th>
                            <th>***********</th>
                            <th class="btn-group">
                                <a href="{{url('edit_page',[$data->id])}}"><button class="btn btn-warning mr-3">Edit</button></a>
                            @if(Session::get('type') == 'admin')
                                <a href="{{url('delete_page',[$data->id])}}"><button class="btn btn-danger">delete</button></a>
                            @else
                            @endif -->
                            </th>
                        </tr>
                        <input type="hidden" value="{{$counter ++}}">
                    @endforeach
                </tbody>
            </table>

        </div>
        @endif
    @else
    <div class="alert alert-info text-center">
        Need to login first to get details
    </div>
    @endif

    @if(Session::has('email'))
    <div class="modal" id="adddetailmodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Add Detail</h2>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <form action="{{url('add_detail_form')}}" method='post'>
                   @csrf
                    <p class="text-center" id="message"></p>
                    <label for="">Full Name</label>
                    <input type="text" value="{{$data->full_name}}" class="form-control" name="fullname" required >
                    <label for="">Email</label>
                    <input type="email" value="{{$data->email}}" class="form-control" name="email" required readonly>
                    <label for="">Job Profile</label>
                    <input type="text" class="form-control" name="job_profile" required>
                    <label for="">Company Name</label>
                    <input type="text" class="form-control" name="company_name" required>
                    <label for="">Duration</label>
                    <input type="number" class="form-control" name="duration" required>
                    <label for="">Position Title</label>
                    <input type="text" class="form-control" name="position_title" required>
                    <label for="">Location</label>
                    <textarea type="text" class="form-control" name="location" required></textarea>
                    <label for="">Technologies</label>
                    <input type="text" class="form-control" name="technologies" required>
                    <label for="">Work_description</label>
                    <textarea type="text" class="form-control" name="work_description" required></textarea>
                    <label for="">Skill</label>
                    <input type="text" class="form-control" name="skill" required>
                    <button class="btn btn-success mt-4">Submit</button>
                   </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    @endif
@endsection
    