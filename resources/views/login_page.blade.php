@extends('header_footer')
@section('main_content')
    <div class="row">
        <div class="col-md-6 mx-auto">
            <h2 class="text-center">Welcome to Laravel Login Page</h2>        
        </div> 
    </div>
    <div class="col-md-6 mx-auto text-center">
        @if(session()->has('error'))
            <div class="alert alert-danger">{{session()->get('error')}}</div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-6 mx-auto bg-dark p-4 text-light">
            <form action="{{url('login_form')}}" method="post">
                @csrf
                <label for="">Email</label>
                <input type="email" class="form-control" name="email" required>
                <label for="">Password</label>
                <input type="password" class="form-control" name="password" required>
                <div class="text-right mt-4">
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#forgotpasswordmodal" class="text-white">Forgot Password?</a><br>
                    <input type="submit" class="btn btn-success mt-4" >
                </div>
            </form>
        </div>
    </div>
    <div class="modal" id="forgotpasswordmodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Forgot Password</h2>
                    <button class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="">Enter your registered Email</label>
                    <p class="text-center" id="message"></p>
                    <input type="text" class="form-control" id="email">
                    <button class="btn btn-success mt-4" onclick="forgot_password()">Submit</button>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <script>
        forgot_password =()=>{
            let email = $('#email').val();
            if(email == ''){
                $('#message').html('Please Enter email then submit').addClass('text-danger')
                return false
            }
            $('#message').html('')
            $.ajax({
                url:'{{url("forgot_password_check")}}',
                type:'get',
                data:{email:email,},
                success:function(data){
                    if(data == 0){
                        $('#message').html('Email is not registered with us. Please check it').addClass('text-danger')
                    }
                    $('#message').html('Reset Password Email is send to your registered mail').addClass('text-success');
                    setTimeout(() => {
                        $('#forgotpasswordmodal').modal('hide')
                    }, 1000);
                }
            })
        }
    
    </script>
@endsection
