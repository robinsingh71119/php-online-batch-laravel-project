@extends('header_footer')
@section('main_content')
    <div class="row">
        <div class="col-md-12 mx-auto">
            <h2 class="text-center">Welcome to Laravel Register Page</h2>        
        </div> 
        @if($errors->any())
        <div class="alert alert-danger mx-auto">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-group-item">
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-6 bg-dark p-4 text-white mx-auto mb-4 mt-4">
            <form action="{{url('register_form')}}" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Full Name</label>
                <input type="text" class="form-control" placeholder="Enter your full name" name="full_name">
                <label for="">Email</label>
                <input type="email" class="form-control" placeholder="Enter your Email" name="email">
                <label for="">Contact No.</label>
                <input type="number" class="form-control" placeholder="Enter your Contact No" name="contact_no">
                <label for="">Password</label>
                <input type="password" class="form-control" placeholder="Enter your Password" name="password">
                <label for="">D.O.B</label>
                <input type="hidden" value="user" name="user_type">
                <input type="date" class="form-control" placeholder="Enter your D.O.B" name="dob">
                <label for="">Profile Image</label>
                <input type="file" class="form-control" name="pro_img">
                <div class="text-center m-4">
                    <input type="submit" class="btn btn-light">
                </div>
            </form>
        </div>
    </div>
@endsection
