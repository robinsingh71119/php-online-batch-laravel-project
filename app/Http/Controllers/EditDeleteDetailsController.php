<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\registration;
use File;
use Session;

class EditDeleteDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_query = registration::find($id);
        return view('edit_page')->with('edit_details',$get_query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo $id;
        echo '<pre>';
        print_r($request->all());
        echo '</pre>';
        $update_get_data = registration::find($id);
        $update_get_data->full_name = $request->full_name;
        $update_get_data->contact_no = $request->contact_no;
        $update_get_data->dob = $request->dob;
        if($request->hasFile('profile_image')){
            $existing_image = public_path('/profile_image'.$update_get_data->profile_image);
            if(File::exists($existing_image)){
                File::delete($existing_image);
            }
            $new_image = $request->file('profile_image');
            $new_name = rand().time().'.'.$new_image->getClientOriginalExtension();
            $destination = public_path('profile_image');
            $new_image->move($destination,$new_name);
            $update_get_data->profile_image = $new_name;
        }
        $update_get_data->save();
        session()->flash('success','Update Successfully');
        return redirect('/');

    }

    public function delete($id){
        // $delete_query = registration::where('id',$id)->first();
        $delete_query = registration::find($id);
        $delete_query->delete();
        session()->flash('success','User Details Deleted successfully');
        return redirect('/');
    }

    public function changePassword(Request $request){
        $get_user_data = registration::where('email',Session::get('email'))->first();
        if(!Hash::check($request->old_password,$get_user_data->password)){
            session()->flash('error','Old Password is not correct');
            return redirect('edit_page/'.$get_user_data->id);
        }else{
            $get_user_data = registration::where('email',Session::get('email'))->update(['password'=>Hash::make($request->new_password)]);
            session()->flash('success','Your Password has changed successfully');
            return redirect('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
