<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\registration;
use App\user_details;
use Session;

class IndexPageController extends Controller
{
    public function index(){
        $get_data = array();
        $get_user_details_data = array();
        if(Session::has('email')){
            $get_email_data = registration::where(['email'=>Session::get('email')])->first();    
            if($get_email_data->type == 'admin'){
                $get_data = registration::all(); 
            }else{
                $get_data = registration::where(['email'=>Session::get('email')])->get(); 
                $get_user_details_data = user_details::where(['email'=>Session::get('email')])->get(); 
            }
        }
        // $get_data = registration::where(['type'=>'user'])->get();
        return view('indexpage')->with('registration_data',$get_data)->with('user_details',$get_user_details_data);
    }
}
