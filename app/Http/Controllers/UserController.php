<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\user_details;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // die();
        $insert_data = user_details::create($data);
        // $insert_user_details = new user_details;
        // $insert_user_details->fullname = $request->fullname;
        // $insert_user_details->email = $request->email;
        // $insert_user_details->job_profile = $request->job_profile;
        // $insert_user_details->company_name = $request->company_name;
        // $insert_user_details->duration = $request->duration;
        // $insert_user_details->position_title = $request->position_title;
        // $insert_user_details->location = $request->location;
        // $insert_user_details->technologies = $request->technologies;
        // $insert_user_details->work_description = $request->work_description;
        // $insert_user_details->skill = $request->skill;
        // $insert_user_details->save();
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
