<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\registration;
use App\password_reset;
use Session;
use Mail;
// use Illuminate\Support\Facaed\Mail;

class RegisterLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'email' => 'unique:registrations|email:rfc,dns',
        ]);

        $obj = new registration();
        $obj->full_name     = $request->full_name;
        $obj->email         = $request->email;
        $obj->contact_no    = $request->contact_no;
        // $obj->password      = $request->password;
        $obj->password      = Hash::make($request->password);
        $obj->dob           = $request->dob;
        $obj->type          = $request->user_type;
        if($request->hasFile('pro_img')){
            $image = $request->file('pro_img');
            $new_name = rand().time().'.'.$image->getClientOriginalExtension();
            $destination = public_path('profile_image');
            $image->move($destination,$new_name);
            $obj->profile_image = $new_name;
        }
        $obj->save();
        session()->flash('success','Registration is successfully done.');
        return redirect('/');
        
    }

    public function check(Request $request){
        $get_login_request_data_query = registration::where(['email'=>$request->email])->first();
        if(!$get_login_request_data_query){
            session()->flash('error','Email is not valid.');
            return redirect('login');
        }else{
           if(Hash::check($request->password,$get_login_request_data_query->password)){
               Session::put('email',$get_login_request_data_query->email);
               Session::put('type',$get_login_request_data_query->type);
               return redirect('/');
           }else{
            session()->flash('error','Password is not correct .');
            return redirect('login');
           }
        }
    }

    public function forgotPassword(Request $request){
        
        $check_email = registration::where('email',$request->email)->first();
        if(!$check_email){
           return 0; 
        }else{
            $token = rand().time();
            $insert_query = new password_reset();
            $insert_query->email = $request->email;
            $insert_query->token = $token;
            $mail_array = array(
                                'email' => $request->email,
                                'token' => $token
                               );
            // Mail::send('bladefilename',arrayholdingalldetails,function(param) usekeyword (arrayholdingalldetails){

            // } );
            Mail::send('email_view.forgot_password_mail',$mail_array,function($m) use ($mail_array){
                $m->to($mail_array['email'])->subject('Password Reset Mail');
                $m->from('Websiteadmin@gmail.com','Reset Password');
            });
            $insert_query->save();
            return 1;
        }
        
    }

    public function resetPassword($token){
        $check_token = password_reset::where('token',$token)->first();
        if(!$check_token){
            session()->flash('error','This Token is expired');
            return redirect('/');
        }
        return view('reset_password')->with('token',$token);
    }

    public function NewPassword(Request $request, $token){
        $get_password_reset_data = password_reset::where('token',$token)->first();
        $hash_password = Hash::make($request->new_pass);
        $get_registration_data = registration::where('email',$get_password_reset_data)->update(['password'=>$hash_password]);
        $get_password_reset_data->delete();
        session()->flash('success','Your Password is successfully Reset');
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Session::flush();
        return redirect('/');
    }

}
