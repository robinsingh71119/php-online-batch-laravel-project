<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_details extends Model
{
    protected $fillable = ['fullname','email','job_profile','company_name','duration','position_title','location','technologies','work_description','skill'];
}
