<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/',function(){
//     return view('indexpage');
// });

Route::get('/','IndexPageController@index');

// sign in route
Route::view('/login','login_page');
Route::post('login_form','RegisterLoginController@check');
Route::get('/logout','RegisterLoginController@logout');

// sign up route
Route::view('register','register_page');
Route::post('register_form','RegisterLoginController@store');


// delete register details route
Route::get('delete_page/{id}','EditDeleteDetailsController@delete');

// edit register details route
Route::get('edit_page/{id}','EditDeleteDetailsController@edit');
Route::post('edit_form/{id}','EditDeleteDetailsController@update');

// forgot password ajax route
Route::get('forgot_password_check','RegisterLoginController@forgotPassword');
Route::get('reset_password/{token}','RegisterLoginController@resetPassword');
Route::post('password_reset_form/{token}','RegisterLoginController@NewPassword');


// Change Password route
Route::post('change_password','EditDeleteDetailsController@changePassword');

// User route
Route::post('add_detail_form','UserController@store');

